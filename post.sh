#!/bin/sh

# Dependencies:
# - find-mirrors.awk

# Important:
# Before running post.sh add ssh key to local and remote (gitlab) host.
#
# A && B || C is not if then else
# shellcheck disable=SC2015
#
# TODO/FIXME:
# - paru in not installed in the compiled-software directory

: "${XDG_CONFIG_HOME:=$HOME/.config}"
readonly \
    LOG_FILE="${HOME}/artix-installer.log" \
    XDG_CONFIG_HOME \
    HOME
ENABLE_AUR_P=

print_exit() {
    printf "%s\n" "$1"
    exit "$2"
}

# Allow word globbing
# shellcheck disable=SC2046
check_ssh_keys() {
    # FIXME: setting up ssh keys does not work
    if [ -z "$(ls -A "$HOME"/.ssh)" ]; then
        printf "Setup an ssh key pair before running %s." "$0"
        exit 1
    fi
    return
}

ask_stuff() {
    printf "Do you want to enable the aur? [y/n] (default no): "
    read -r ENABLE_AUR_P
    readonly ENABLE_AUR_P
}

download_dependencies() {
    gitlab="https://gitlab.com/bryanrinders/artix-installer/-/raw/main"
    cd "${HOME}" \
        && wget "${gitlab}"/utils/update-mirrors "${gitlab}"/doc/pkg-com "${gitlab}"/doc/pkg-aur \
        && chmod a+x ./update-mirrors \
        && sudo mv -i ./update-mirrors /usr/local/bin/ \
        || print_exit "Failed downloading artix-installer files/scripts." 2
}

_clone_install() {  # clone [and install] my repos
    # Usage: $0 name_gitlab_repo
    cd "${HOME}"/my-projects/ \
        || { printf "_clone_install: failed to cd into my-projects directory" >> "${LOG_FILE}"
            return 1
        }
    git clone git@gitlab.com:bryos/"$1".git \
        && cd "$1" \
        && case "$1" in
            st|dmenu|dwm|dwmblocks) sudo make clean install ;;
            gruvbox-gtk-theme|dotfiles) sh install.sh ;;
            *)
           esac \
        || printf "Failed to clone/install %s.\n" "$1" >> "${LOG_FILE}"
    return
}

dotfiles() {
    mkdir -pv "${HOME}"/my-projects
    sudo pacman -S --noconfirm --needed git
    _clone_install dotfiles

    # To fix nvim not being found when executing: sudo vim <file>
    [ ! -f /usr/bin/vim ] \
        && sudo ln -s /usr/bin/nvim /usr/bin/vim \
        || printf "Failed to symlink nvim to vim, /usr/bin/vim already exists.\n" >> "${LOG_FILE}"

    # Make sure the root user uses my configs
    sudo mkdir -pv /root/.config \
        && sudo -E ln -st /root/.config/ "${HOME}/.config/nvim" "${HOME}/.config/bash" \
        || printf "Failed to symlink /root/.confg/{nvim,bash}.\n" >> "${LOG_FILE}"

    return
}

enable_arch() {
    # Requires that the Universe repo is enbled.
    sudo pacman -S --needed --noconfirm artix-archlinux-support \
        && sudo pacman-key --populate archlinux \
        && sudo pacman -Syyu \
        || printf "Failed to enable arch repos.\n" >> "${LOG_FILE}"
    return
}

mirrors() {
    sudo pacman -S --noconfirm --needed pacman-contrib \
        && sudo update-mirrors \
        || print_exit "Failed to update mirrorlist.\n" 3
    return
}

pkg_install_com() {
    # sudo doesn't affect redirects
    # shellcheck disable=SC2024
    sudo pacman -S --needed --noconfirm - < "${HOME}/pkg-com" \
        || printf "Failed to install official packages.\n" >> "${LOG_FILE}"
    return
}

install_from_source() { # Installing personal repos
    mkdir -pv "${HOME}"/my-projects
    _clone_install dwm
    _clone_install dwmblocks
    _clone_install dmenu
    _clone_install st
    _clone_install gruvbox-gtk-theme
    _clone_install wallpapers
    return
}

arch_user_repos() {
    [ "${ENABLE_AUR_P}" = 'y' ] || return 1

    mkdir -pv "${HOME}"/.local/compiled-software \
    cd "${HOME}"/.local/compiled-software \
        && sudo pacman -S base-devel --needed --noconfirm \
        && git clone https://aur.archlinux.org/paru.git \
        && cd paru \
        && makepkg -si --noconfirm --needed \
        || { printf "Failed to enable the AUR.\n" >> "${LOG_FILE}"; return 1; }

    paru -S --needed --noconfirm - < "${HOME}/pkg-aur" \
        || printf "Failed to install AUR packages.\n" >> "${LOG_FILE}"
    return
}

edit_fstab() {
    mnt_dir='/home/br/documents/mnt'
    mnt_options='rw,relatime,nofail,user,uid=1000,fmask=113,dmask=002'
    { cat << EOF
UUID=1653-E4AF        $mnt_dir/verbatim	vfat $mnt_options  0 2
UUID=51AA-0F0         $mnt_dir/ereader	vfat $mnt_options  0 2
UUID=4E1AEA7B1AEA6007 $mnt_dir/elements ntfs $mnt_options  0 2
EOF
    } | sudo tee -a /etc/fstab \
        || printf "Failed to configure fstab.\n" >> "${LOG_FILE}"
    return
}

# Allow word globbing
# shellcheck disable=SC2086
clean_up() {
    to_trash='.bash_profile
              .bash_history
              .bash_logout
              .bashrc
              .wget-hsts
              /usr/local/bin/update-mirrors
              pkg-com
              pkg-aur'
    cd "${HOME}" \
        && sudo rm -f $to_trash \
        || printf "Failed to remove default configs and artix-installer doc/utils.
Check these files:\n\t%s\n" "$(echo $to_trash | sed 's/ /\n\t/')" >> "${LOG_FILE}"
    return
}

main() {
    check_ssh_keys
    ask_stuff
    download_dependencies
    dotfiles
    enable_arch
    mirrors
    pkg_install_com
    install_from_source
    arch_user_repos
    edit_fstab
    clean_up
}

main
