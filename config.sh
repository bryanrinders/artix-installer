#!/usr/bin/env sh

# Exit on error and trace output
set -ex

# Global variables
readonly SYS_INFO=/root/sys_info
read -r USER UPWD RPWD HOST DUALBOOT DISK _ < "${SYS_INFO}"
readonly USER UPWD RPWD HOST DUALBOOT DISK

welc_msg() {
    cat << EOF

----------------------------------------------------------------------------------------------------
--------------------------------- Configuration of the Base System ---------------------------------
----------------------------------------------------------------------------------------------------

EOF
}

check_internet() {
    ping artixlinux.org -c 1
}

setup_pacman() {
    pacman-key --init
    pacman-key --populate artix
}

set_timezone_systemclock() {
    ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
    hwclock --systohc
}

localization() {
    sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
    locale-gen
    echo 'LANG=en_US.UTF-8' > /etc/locale.conf
}

boot_loader() {
    pacman -S --noconfirm --needed grub

    if [ "${DUALBOOT}" = 'yes' ]; then
        pacman -S --noconfirm --needed ntfs-3g os-prober
        cp /etc/default/grub /etc/default/grub.bak
        sed -i 's/#\?\(GRUB_DISABLE_OS_PROBER\)=\(false\|true\)/\1=false/' /etc/default/grub
    fi

    if [ -d /sys/firmware/efi/efivars/ ]; then
        # UEFI systems
        pacman -S --noconfirm --needed efibootmgr
        grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
    else # BIOS
        grub-install --recheck "${DISK}"
    fi

    grub-mkconfig -o /boot/grub/grub.cfg
}

manage_users() {
    printf "%s\n%s" "${RPWD}" "${RPWD}" | passwd root

    useradd --create-home "${USER}"
    printf "%s\n%s" "${UPWD}" "${UPWD}" | passwd "${USER}"

    usermod -aG users,wheel "${USER}"
    sed -i 's/# \?\(%wheel ALL=(ALL:ALL) NOPASSWD: ALL\)/\1/' /etc/sudoers
}

networking() {
    printf "%s" "${HOST}" > /etc/hostname

    sed -i "s/{}/${HOST}/g" /root/hosts
    sudo mv /root/hosts /etc/

    pacman -S --noconfirm --needed networkmanager networkmanager-runit neovim curl wget

    ln -s /etc/runit/sv/NetworkManager /etc/runit/runsvdir/current
}

main() {
    welc_msg
    check_internet
    setup_pacman
    set_timezone_systemclock
    localization
    boot_loader
    manage_users
    networking
}

main
