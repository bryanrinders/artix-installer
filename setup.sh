#!/usr/bin/env bash

#+TITLE: Artix preinstallation script
#+AUTHOR: Bryan Rinders

# Exit on error and trace output
set -ex

# Global variables
USER='br'
UPWD=
RPWD=
HOST=
DUALBOOT=
DISK='/dev/sda'
SWAP_PART=
BOOT_PART=
ROOT_PART=
HOME_PART=
SWAP_SIZE=8   # in GiB
ROOT_SIZE=40  # in GiB
BOOT_SIZE=512 # in MiB
readonly SYS_INFO=/root/sys_info

usage() {
    cat << EOF
Usage: $0 [ N ]
Where N is one of:
    1: Answer basic information about how your to be installed system.
    2: Partition the disk.
    3: Format the partitions
    4: Mount the partitions
    5: Install the base
    6: Generate /etc/fstab
    7: Chroot
    8: Run the entire pre-installation script (i.e. all the above parts).
Any other CL argument or lack there of will print this message.
EOF
}

# shellcheck disable=SC2015
download_scripts() {
    files='config.sh /doc/hosts'
    for file in ${files}; do
        wget https://gitlab.com/bryanrinders/artix-installer/-/raw/main/config.sh
    done
}

welcome_message() {
    cat << EOF

----------------------------------------------------------------------------------------------------
------------------------------------------ Pre-installation ----------------------------------------
----------------------------------------------------------------------------------------------------

EOF
}

save_system_info() {
    echo "${USER} ${UPWD} ${RPWD} ${HOST} ${DUALBOOT} ${DISK} ${SWAP_SIZE} \
        ${BOOT_SIZE} ${ROOT_SIZE}" > "${SYS_INFO}"
}

ask_system_info() {
    for info in "$@"; do
        case "${info}" in
            1|user)
                read -p "Enter username to be created: " -r USER
                ;;
            2|userpwd)
                read -p "Enter new password for ${USER}: " -r UPWD
                RPWD="${UPWD}"  # use the users password also for root
                ;;
            3|hostname)
                read -p "Enter new hostname (device name): " -r HOST
                ;;
            4|dualboot)
                read -p "Is this a dual boot? Yes/no (default no): " -r DUALBOOT
                : "${DUALBOOT:=no}"
                ;;
            5|disk)
                printf "\nWhere do you want to install Artix?\n\n"
                lsblk
                read -p "\nEnter full path to the disk, e.g. /dev/sda (default /dev/sda): " -r DISK
                : "${DISK:=/dev/sda}"
                ;;
            6|swapsize)
                read -p "How large do you want the swap partition (in GB): " -r SWAP_SIZE
                ;;
            7|bootsize)
                read -p "How large do you want the boot partition (in MB): " -r BOOT_SIZE
                ;;
            8|rootsize)
                read -p "How large do you want the root partition (in GB): " -r ROOT_SIZE
                ;;
            *) printf "Not an option: %s\n" "${info}"
        esac
    done
}

basic_system_info() { # Ask for username, password, etc
    nvme_part=
    change_options=

    if [ -e "${SYS_INFO}" ]; then
        read -r USER UPWD RPWD HOST DUALBOOT DISK SWAP_SIZE BOOT_SIZE ROOT_SIZE < "${SYS_INFO}"
    else
        ask_system_info userpwd hostname dualboot disk rootsize
    fi

    while :; do
        # FIXME: home_size, compute size of home partition.
        #home_size="$(lsblk -o NAME,SIZE | awk "/^${DISK}/ { print \$2 }")"
        #home_size="${home_size%%[T|G|M|K]}"
        #home_size="$(( home_size - SWAP_SIZE - BOOT_SIZE / 1000 - ROOT_SIZE ))"
        cat << EOF

########################################################################################
Current settings:
1. User: ${USER}
2. User/Root password: ${UPWD}
3. Hostname: ${HOST}
4. Dual boot: ${DUALBOOT}
5. Disk: ${DISK}
6. Size of the swap partition: ${SWAP_SIZE}GB
7. Size of the boot partition: ${BOOT_SIZE}MB
8. Size of the root partition: ${ROOT_SIZE}GB
NOTE: this means the HOME partition will have a size of ${home_size}

Give a space seperated list of the numbers of the settings to change or 'y' to continue:

EOF

        read -r change_options
        [ "${change_options}" = 'y' ] && break
        # dont quote change_options to allow word globbing
        # shellcheck disable=SC2086
        ask_system_info ${change_options}
    done

    readonly USER UPWD RPWD HOST DUALBOOT DISK SWAP_SIZE BOOT_SIZE ROOT_SIZE

    case "${DISK}" in
        /dev/nvme*) nvme_part='p' ;;
        *)
    esac

    readonly SWAP_PART="${DISK}${nvme_part}1"
    readonly BOOT_PART="${DISK}${nvme_part}2"
    readonly ROOT_PART="${DISK}${nvme_part}3"
    readonly HOME_PART="${DISK}${nvme_part}4"

    unset nvme_part change_options home_size 

    save_system_info
}

# shellcheck disable=SC2015
sfdisk_partitioning() {
    boottype="linux"
    labeltype="dos"
    [ -d "/sys/firmware/efi" ] && { boottype="uefi"; labeltype="gpt"; }

    echo "label: ${labeltype}" | sfdisk "${DISK}" #\
    echo "\
size=${SWAP_SIZE}G, type=swap,        name=SWAP
size=${BOOT_SIZE}M, type=${boottype}, name=BOOT, bootable
size=${ROOT_SIZE}G, type=linux,       name=ROOT
size=+,             type=linux,       name=HOME" \
            |  sfdisk "${DISK}" #\
}

# shellcheck disable=SC2015
format_partitions() {
    if [ -d "/sys/firmware/efi" ] ; then
        # UEFI
        echo 'Detected UEFI system'
        mkfs.fat -F 32 "${BOOT_PART}" #\
        fatlabel "${BOOT_PART}" BOOT #\
    else
        # BIOS
        echo 'Detected BIOS system'
        mkfs.ext4 -L BOOT "${BOOT_PART}" #\
    fi

    mkfs.ext4 -L ROOT "${ROOT_PART}" #\
    mkfs.ext4 -L HOME "${HOME_PART}" #\
    mkswap -L SWAP "${SWAP_PART}" #\
}

# shellcheck disable=SC2015
mount_partitions() {
    mount /dev/disk/by-label/ROOT /mnt
    mkdir /mnt/boot /mnt/home
    mount /dev/disk/by-label/HOME /mnt/home
    mount /dev/disk/by-label/BOOT /mnt/boot

    swapon /dev/disk/by-label/SWAP
}

# shellcheck disable=SC2015
install_base() {
    basestrap /mnt base base-devel runit elogind-runit linux-lts \
            linux-firmware --noconfirm --needed
}

generate_fstab() {
    fstabgen -U /mnt >> /mnt/etc/fstab #\
}

enter_systen() {
    # Copy the files to destination system's root partition
    cp -r "${SYS_INFO}" /root/config.sh /root/hosts /mnt/root/
    chmod a+x /mnt/root/config.sh

    # run config script from inside of chroot
    artix-chroot /mnt /root/config.sh

    rm /mnt"${SYS_INFO}" /mnt/root/config.sh
    umount -R /mnt
    echo "You can now reboot your machine."
}

main() {
    welcome_message
    basic_system_info
    download_scripts
    sfdisk_partitioning
    format_partitions
    mount_partitions
    install_base
    generate_fstab
    enter_systen
}

main
exit
